import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

/** Example of a Servlet that gets an ISBN number and returns the book price
 */
@WebServlet(
  description = "converts temperature",
  urlPatterns = {"/convert"}
)
public class Convert extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
  public void init() throws ServletException {
  }	
  public double celToFar(double celsius){
    return celsius * 1.8 + 32.0;
  }
  public void doGet(
    HttpServletRequest request,
    HttpServletResponse response
  )
  throws ServletException, IOException
  {
    String celsiusString = request.getParameter("celsius");
    if (celsiusString == null){
        response.sendError(400, "missing url parameter: celsius");
        return;
    }
    double celsius;
    try {
        celsius = Double.parseDouble(celsiusString);
    } catch (NumberFormatException e) {
        response.sendError(400, "given celsius is not a number");
        return;
    }
    double farenheit = this.celToFar(celsius);
    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String docType = "<!DOCTYPE HTML>\n";
    String title = "Temperature converter";
    out.println(docType +
      "<HTML>\n" +
      "<HEAD><TITLE>" + title + "</TITLE>" +
      "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
      "</HEAD>\n" +
      "<BODY BGCOLOR=\"#FDF5E6\">\n" +
      "<H1>" + title + "</H1>\n" +        
      "<P>Celsius: " + celsius + "</p>" +     
      "<P>Farenheit: " + farenheit + "</p>\n" +
      "<a href='./'>convert next</a>" +
      "</BODY></HTML>");
    
  }
  
}
